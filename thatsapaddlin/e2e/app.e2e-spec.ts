import { ThatsapaddlinPage } from './app.po';

describe('thatsapaddlin App', () => {
  let page: ThatsapaddlinPage;

  beforeEach(() => {
    page = new ThatsapaddlinPage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('app works!');
  });
});

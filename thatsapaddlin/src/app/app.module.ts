import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

import { MaterialModule, MdNativeDateModule  } from '@angular/material';
import { MdButtonModule, MdCardModule, MdMenuModule, MdToolbarModule, MdIconModule, MdSidenavModule, MdListModule, MdDialogModule } from '@angular/material';

import 'hammerjs';

import { AppComponent, SignInDialog } from './app.component';

import { GoogleAuthService } from './services/google-auth.service';

@NgModule({
    declarations: [
        AppComponent,
        SignInDialog
    ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        FormsModule,
        HttpModule,
        MaterialModule,
        MdNativeDateModule,
        MdButtonModule,
        MdMenuModule,
        MdCardModule,
        MdToolbarModule,
        MdIconModule,
        MdSidenavModule,
        MdListModule,
        MdDialogModule
    ],
    providers: [
        GoogleAuthService
    ],
    bootstrap: [
        AppComponent,
        SignInDialog
    ]
})
export class AppModule { }

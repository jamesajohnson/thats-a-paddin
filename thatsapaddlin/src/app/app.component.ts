import { Component, Optional, OnInit, NgZone } from '@angular/core';
import { MdDialog } from '@angular/material';

import { GoogleAuthService } from './services/google-auth.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    title = 'app works!';

    constructor (private _ngZone: NgZone, public dialog: MdDialog, private googleAuthService: GoogleAuthService) {
        window['angularComponentRef'] = {component: this, zone: _ngZone};
    }

    ngOnInit() {
        this.showSigninDialog();
    }

    showSigninDialog(): void {
        this.dialog.open(SignInDialog);
    }

    public onSignIn = (googleUser) => {
        this.googleAuthService.setProfile(googleUser);
    }

    public signOut(): void {
        /*var auth2 = gapi.auth2.getAuthInstance();
        auth2.signOut().then(function () {
            console.log('User signed out.');
        });*/
    }
}

@Component({
  selector: 'google-sign-in-dialog',
  templateUrl: 'google-sign-in-dialog.html',
})
export class SignInDialog {
    constructor(private googleAuthService: GoogleAuthService) {}
}

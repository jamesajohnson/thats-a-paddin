import { Injectable } from '@angular/core';

@Injectable()
export class GoogleAuthService {

    profile;
    userName: string = '';
    userId: string = '';
    userEmail: string = '';
    userImageUrl: string = '';

    constructor() { }

    public isSignedIn(): boolean {
        return this.userId.length > 0;
    }

    public setProfile(googleUser): void {
        this.profile = googleUser.getBasicProfile();
        this.userName = this.profile.getName();
        this.userId = this.profile.getId();
        this.userEmail = this.profile.getEmail();
        this.userImageUrl = this.profile.getImageUrl();
        console.log('ID: ' + this.userName); // Do not send to your backend! Use an ID token instead.
        console.log('Name: ' + this.userId);
        console.log('Image URL: ' + this.userEmail);
        console.log('Email: ' + this.userImageUrl); // This is null if the 'email' scope is not present
    }

    public getUserName(): string {
        return this.userName;
    }

    public getUserFirstname(): string {
        return this.userName.split(' ')[0];
    }

}

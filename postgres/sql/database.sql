/**
* Create the wall table.
* Stores each walls basic information
*/

DROP TABLE IF EXISTS walls;

CREATE TABLE walls (
  wall_id SERIAL PRIMARY KEY,
  wall_name VARCHAR(50)
);

/**
* Create products table
*/

DROP TABLE IF EXISTS products;

CREATE TABLE products (
  product_id SERIAL PRIMARY KEY,
  product_name VARCHAR(50),
  product_price DECIMAL(12, 2),
  product_description VARCHAR(2000),
  product_productTypeID INT
);

/**
* Create productTypes table
*/

DROP TABLE IF EXISTS productTypes;

CREATE TABLE productTypes (
  producttype_ID SERIAL PRIMARY KEY,
  producttype_title VARCHAR(50)
);


/**
* Create wall products table
*/

DROP TABLE IF EXISTS wallProducts;

CREATE TABLE wallProducts (
  wallproduct_id SERIAL PRIMARY KEY,
  product_id INT,
  wall_id INT
);

/**
* Create members table
*/

DROP TABLE IF EXISTS members;

CREATE TABLE members  (
  member_id SERIAL PRIMARY KEY,
  member_firstname VARCHAR(20),
  member_middlename VARCHAR(20),
  member_lastname VARCHAR(20),
  member_dob DATE
);

/**
* Create membership typees table
*/
DROP TABLE IF EXISTS membershipTypes;

CREATE TABLE memberships (
  membership_id SERIAL PRIMARY KEY,
  membership_title VARCHAR(20)
);

/**
* Create table of member memberships
*/
DROP TABLE IF EXISTS memberMemberships;

CREATE TABLE memberMemberships (
  membermembership_id SERIAL PRIMARY KEY,
  member_id INT,
  membership_id INT
);

INSERT INTO walls (wall_name) VALUES
('Everest Climbing Wall'),
('Rock Town Bouldering');

INSERT INTO productTypes (producttype_title) VALUES
('Membership'),
('Entry'),
('Equipment Hire');

INSERT INTO products (product_name, product_price, product_description, product_productTypeID) VALUES
('1 Month Membership', 35.00, 'None', 1),
('3 Month Membership', 90.00, 'None', 1),
('1 Years Membership', 250.00, 'None', 1),
('10 Climb Punch Card', 60.00, 'None', 1),
('Adult Peak Climbing', 8.00, 'None', 2),
('Adult Off Peak Climbing', 7.00, 'None', 2),
('Student Climbing', 7.00, 'None', 2),
('under 7s Climbing', 5.00, 'None', 2);

INSERT INTO members (member_firstname, member_middlename, member_lastname, member_dob) VALUES
('James', 'Alexander', 'Johnson', '1994-09-13');

INSERT INTO members (member_firstname, member_lastname, member_dob) VALUES
('Alex', 'Honold', '1985-08-17'),
('Tommy', 'Caldwell', '1978-08-11'),
('Dean', 'Potter', '1972-04-14');

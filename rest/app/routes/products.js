const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM products`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
});

router.route('/:product_id')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            var product_id = req.params.product_id
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM products WHERE product_id = ${product_id}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .patch(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(updateQuery('products', req.body, req.params.product_id), [], function (err, results) {
                done();

                if (err) {
                    console.log(err);
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    })
    .delete(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(`DELETE FROM products WHERE product_id = ${req.params.product_id}`, [], function (err, results) {
                done();

                if (err) {
                    console.log(err);
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    });

/**
 * Build an SQL Statement to update the provided table
 *
 * @param tableName   The table to update
 * @param data        Array of data to update the table with, array keys should be names after table columns
 * @param product_id  The ID of the product to update
*/

function updateQuery(tableName, data, product_id) {
    var query = `UPDATE ${tableName} SET `;
    var columnUpdates = [];

    Object.keys(data).forEach( key => {
        if (typeof data[key] === 'string') {
            columnUpdates.push(`${key} = '${data[key].replace("'","''")}'`);
        } else {
            columnUpdates.push(`${key} = '${data[key]}'`);
        }
    });

    query += columnUpdates.join(', ');
    query += `WHERE product_id = ${product_id}`;

    return query;
}

module.exports = router

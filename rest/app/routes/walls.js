const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .post(function(req, res) {
        const wall = req.body

        pg.connect(conString, function (err, client, done) {
            if (err) {
                // pass the error to the express error handler
                console.log(err)
                return next(err)
            }
            client.query(`INSERT INTO walls (name) VALUES ($1);`, [wall.name], function (err, result) {
                done() //signal that the connect can be closed

                if (err) {
                    // pass the error to the express error handler
                    return next(err)
                }

                res.sendStatus(200) //return success code
            })
        })
    })
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM walls`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
});

router.route('/:id')
    .get(function(req, res) {

        pg.connect(conString, function (err, client, done) {
            var id = req.params.id
            client.query(`SELECT * FROM walls WHERE id = ${id}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {
        var id = req.params.id
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`DELETE FROM walls WHERE id = ${id}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.sendStatus(200) //return results in json format
            })
        })
    })

module.exports = router

const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM members`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
});

router.route('/:member_id')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            var member_id = req.params.member_id
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM members WHERE member_id = ${member_id}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .patch(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(updateQuery('members', req.body, req.params.member_id), [], function (err, results) {
                done();

                if (err) {
                    console.log(err);
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    })
    .delete(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err);
                return next(err);
            }
            client.query(`DELETE FROM members WHERE member_id = ${req.params.member_id}`, [], function (err, results) {
                done();

                if (err) {
                    console.log(err);
                    return next(err);
                }

                res.sendStatus(200);
            });
        });
    });

/**
 * Build an SQL Statement to update the provided table
 *
 * @param tableName   The table to update
 * @param data        Array of data to update the table with, array keys should be names after table columns
 * @param member_id  The ID of the member to update
*/

function updateQuery(tableName, data, member_id) {
    var query = `UPDATE ${tableName} SET `;
    var columnUpdates = [];

    Object.keys(data).forEach( key => {
        if (typeof data[key] === 'string') {
            columnUpdates.push(`${key} = '${data[key].replace("'","''")}'`);
        } else {
            columnUpdates.push(`${key} = '${data[key]}'`);
        }
    });

    query += columnUpdates.join(', ');
    query += `WHERE member_id = ${member_id}`;

    return query;
}

module.exports = router

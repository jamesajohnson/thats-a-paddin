const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM productTypes`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
});

router.route('/:productTypeID')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            var productTypeID = req.params.productTypeID
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM productTypes WHERE productTypeID = ${productTypeID}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {

    })

router.route('/:productTypeID/products')
    .get(function(req, res) {
        pg.connect(conString, function (err, client, done) {
            var productTypeID = req.params.productTypeID
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`SELECT * FROM products WHERE productTypeID = ${productTypeID}`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })

module.exports = router

const pg = require('pg')
const express = require('express')
const router = express.Router({mergeParams: true})

const conString = 'postgres://postgres:password@postgresql/test'

/**
* Middleware for every request.
* Get access key from request and build a list of walls
* the key can access, then put the list into the request.
*/
router.use(function(req, res, next) {
    next()
})

router.get('/', (req, res) => {
    res.send('PostgreSQL')
})

// routes
router.use('/walls', require('./walls'))
router.use('/products', require('./products'))
router.use('/producttypes', require('./productTypes'))
router.use('/members', require('./members'))

module.exports = router

const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .get(function(req, res) {
        // Create array of ids the access token has access to
        let hasAccessToWalls = []
        for (let access of req.access) {
            hasAccessToWalls.push(access.id)
        }

        let statement = `
        SELECT * FROM wallMemberCertifications
        WHERE id = ANY(ARRAY[${hasAccessToWalls.toString()}])
        AND id = ${req.params.id};`

        if (req.params.memberID) {
            statement = `
            SELECT * FROM wallMemberCertifications
            WHERE wallMemberID = ${req.params.memberID}
            AND id = ANY(ARRAY[${hasAccessToWalls.toString()}])
            AND id = ${req.params.id};`
        }

        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(statement, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
});

router.route('/:id')
    .get(function(req, res) {
        // Create array of ids the access token has access to
        let hasAccessToWalls = []
        for (let access of req.access) {
            hasAccessToWalls.push(access.id)
        }

        pg.connect(conString, function (err, client, done) {
            var id = req.params.id
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`
                SELECT * FROM wallMemberCertifications
                WHERE id = ${id}
                AND id = ANY(ARRAY[${hasAccessToWalls.toString()}])
                AND id = ${req.params.id};`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {
        var id = req.params.id
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`
                DELETE FROM wallMemberCertifications
                WHERE id = ${id};`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.sendStatus(200) //return results in json format
            })
        })
    });

module.exports = router

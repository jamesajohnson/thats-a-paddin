const express = require('express')
const router = express.Router({mergeParams: true})
const pg = require('pg')

const conString = 'postgres://postgres:password@postgresql/test'

router.route('/')
    .post(function(req, res) {
        const wallMember = req.body

        pg.connect(conString, function (err, client, done) {
            if (err) {
                // pass the error to the express error handler
                console.log(err)
                return next(err)
            }
            client.query(`
            INSERT INTO wallMembers (name, age)
            VALUES ($1, $2);`, [wallMember.name, wallMember.age], function (err, result) {
                done() //signal that the connect can be closed

                if (err) {
                    // pass the error to the express error handler
                    return next(err)
                }

                res.sendStatus(200) //return success code
            })
        })
    })
    .get(function(req, res) {
        // Create array of ids the access token has access to
        let hasAccessToWalls = []
        for (let access of req.access) {
            hasAccessToWalls.push(access.id)
        }

        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`
                SELECT * FROM wallMembers
                WHERE id = ANY(ARRAY[${hasAccessToWalls.toString()}])
                AND id = ${req.params.id};`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
})

router.route('/:memberID')
    .get(function(req, res) {
        // Create array of ids the access token has access to
        let hasAccessToWalls = []
        for (let access of req.access) {
            hasAccessToWalls.push(access.id)
        }

        pg.connect(conString, function (err, client, done) {
            var memberID = req.params.memberID
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`
                SELECT * FROM wallMembers
                WHERE id = ${memberID}
                AND id = ANY(ARRAY[${hasAccessToWalls.toString()}])
                AND id = ${req.params.id};`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.json(result.rows) //return results in json format
            })
        })
    })
    .put(function(req, res) {

    })
    .delete(function(req, res) {
        var memberID = req.params.memberID
        pg.connect(conString, function (err, client, done) {
            if (err) {
                console.log(err)
                return next(err)
            }
            client.query(`
                DELETE FROM members
                WHERE id = ${memberID}
                AND id = ${req.params.id};`, [], function (err, result) {
                done()

                if (err) {
                    console.log(err)
                    return next(err)
                }

                res.sendStatus(200) //return results in json format
            })
        })
    })

module.exports = router
